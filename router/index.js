import express from 'express';
import json from 'body-parser';
import alumnosdb from '../models/alumnos.js';

export const router = express.Router();
export default {router};

// Se declara primera ruta por omisión
router.get('/', (req, res) => {
    res.render('index', {titulo: "Mis prácticas JS", nombre: "Jaime Rodriguez"});
});

router.get('/table', (req, res) => {
    // Parámetros
    const params = {
        numero: req.query.numero,
    }
    res.render('table', params);
});

router.post('/table', (req, res) => {
    // Parámetros
    const params = {
        numero: req.body.numero,
    }
    res.render('table', params);
});

router.post('/cotizacion',(req, res)=>{
    //Parámetros
    const params ={
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazos: req.body.plazos,
    }
    res.render('cotizacion',params)
});

router.get('/cotizacion', (req,res)=>{
    //Parámetros
    const params = {
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazos: req.query.plazos,
    }
    res.render('cotizacion', params);
});

let rows;
router.get('/alumnos', async(req, res)=>{


        rows = await alumnosdb.mostrartodos();
        res.render('alumnos',{reg:rows});

});

let params;
router.post('/alumnos', async(req, res)=>{
    try{
        params={
            matricula:req.body.matricula,
            nombre:req.body.nombre,
            domicilio:req.body.domicilio,
            sexo:req.body.sexo,
            especialidad:req.body.especialidad
        
        }
        const res =  await alumnosdb.insertar(params);
    } catch (error){
        console.log(error);
        res.status(400).send("sucedio un error"+ error);
    }
    rows = await alumnosdb.mostrartodos();
    res.render('alumnos',{reg:rows});
});